var SerialPort = require('serialport');
var http = require('http');

var serialPort = new SerialPort("/dev/cu.usbmodem1411", {
  baudrate: 96000
});

// it opens the connection and register an event 'data'
serialPort.on("open", function () {
  console.log('Communication is on!');

  // when your app receives data, this event is fired
  // so you can capture the data and do what you need
  serialPort.on('data', function(data) {
  	postPress();
    console.log('data received: ' + data);
  });
});

var postPress = function() {
	var options = {
	  host: '127.0.0.1',
	  port: 80,
	  path: '/resource?id=foo&bar=baz',
	  method: 'POST'
	};

	http.request(options, function(res) {
	  console.log('STATUS: ' + res.statusCode);
	  console.log('HEADERS: ' + JSON.stringify(res.headers));
	  res.setEncoding('utf8');
	  res.on('data', function (chunk) {
	    console.log('BODY: ' + chunk);
	  });
	}).end();	
}
